;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Niklas Eklund <niklas.eklund@posteo.net>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (negnu services mbpfan)
  #:use-module (gnu packages linux)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services shepherd)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:use-module (guix modules)
  #:export (mbpfan-service-type

            mbpfan-configuration
            mbpfan-configuration?))

;;; Commentary:
;;;
;;; This module provides a service definition for the mbpfan.
;;;
;;; Code:

(define-record-type* <mbpfan-configuration>
  mbpfan-configuration make-mbpfan-configuration
  mbpfan-configuration?
  (log-file mbpfan-configuration-log-file ;string
            (default "/var/log/mbpfan.log"))
  (verbose? mbpfan-configuration-verbose? (default #f)))

(define mbpfan-shepherd-service
  (match-lambda
    (($ <mbpfan-configuration> log-file verbose?)
     (list
      (shepherd-service
       (documentation "Run the mbpfan daemon.")
       (provision '(mbpfan))
       (requirement '(syslogd))
       (start #~(make-forkexec-constructor
                 (list #$(file-append mbpfan "/sbin/mbpfan")
                       "-f"
                       #$@(if verbose?
                            '("-v") ; verbose logging
                            '()))
                 #:log-file #$log-file))
       (stop #~(make-kill-destructor)))))))

(define mbpfan-service-type
  (service-type
   (name 'mbpfan)
   (extensions
    (list (service-extension shepherd-root-service-type mbpfan-shepherd-service)))
   (description
    "Run the MacBook/Macbook Pro fan daemon.")
   (default-value (mbpfan-configuration))))

;;; mbpfan.scm ends here
