;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Niklas Eklund <niklas.eklund@posteo.net>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but


;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (negnu packages emacs)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system emacs)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages emacs-xyz))

;;;
;;; Emacs hacking.
;;;

(define-public emacs-recentd-git
  (let ((branch "master")
        (commit "78f3b6ca8773c2a1acb49eb62d4b6ab263896def")
        (revision "0"))
    (package
      (name "emacs-recentd-git")
      (version (git-version "0.1" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/niklaseklund/recentd")
               (commit commit)))
         (sha256
          (base32
           "1l1bd184lgrqxrl2v8s553chkfkang78afb1lfvffkjmqfir7ghg"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (native-inputs
       `(("emacs-ert-runner" ,emacs-ert-runner)))
      (arguments
       `(#:tests? #t
       #:test-command '("ert-runner")))
      (home-page "https://gitlab.com/niklaseklund/recentd")
      (synopsis "Global directory history")
      (description "Recentd provides a history of recently visited
directories.  It integrates with dired and shell.")
      (license license:gpl3+))))

(define-public emacs-modal-polyglot-git
  (let ((branch "main")
        (commit "6b40cf253516468a7c74aeb7fa591b1d564f4f03")
        (revision "0"))
    (package
      (name "emacs-modal-polyglot-git")
      (version (git-version "0.1" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/niklaseklund/modal-polyglot")
               (commit commit)))
         (sha256
          (base32
           "0xwfs3k7a1i02kx4rwwyd0f240fh7x20iin57p6pwq8rfbinn3qn"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (home-page "https://gitlab.com/niklaseklund/modal-polyglot")
      (synopsis "Modal editing for polyglots")
      (description "A convenient package for modal polyglots.")
      (license license:gpl3+))))

(define-public emacs-python-pytest-git
  (let ((commit "f9a0ceae3780b45f6ddfebd8cf0e6191906e4109")
        (revision "0"))
    (package
      (name "emacs-python-pytest-git")
      (version (git-version "3.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/niklascarlsson/emacs-python-pytest")
               (commit commit)))
         (sha256
          (base32
           "1ipk6139fjidkwxcqwnvhvmqn767fs69xirzpkgq7ivaxnl21z03"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs
       `(("emacs-dash" ,emacs-dash)
         ("emacs-projectile" ,emacs-projectile)
         ("emacs-transient" ,emacs-transient)
         ("emacs-s" ,emacs-s)))
      (arguments
       `(#:phases (modify-phases %standard-phases
                    (replace 'install
                      (lambda* (#:key outputs #:allow-other-keys)
                        (let* ((out (assoc-ref outputs "out")))
                          (install-file "python-pytest.el" (string-append out "/share/emacs/site-lisp")))
                        #t)))))
      (home-page "https://github.com/niklascarlsson/emacs-python-pytest")
      (synopsis "run pytest inside Emacs")
      (description "TBD")
      (license license:gpl3+))))

(define emacs-eglot-from-git
  (lambda* (#:key name version revision commit checksum)
    (package
      (inherit emacs-eglot)
      (name name)
      (version (git-version version revision commit))
      (source
       (origin
         (inherit (package-source emacs-eglot))
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/joaotavora/eglot")
               (commit commit)))
         (sha256
          (base32 checksum))
         (file-name (git-file-name name version)))))))

(define-public emacs-eglot-git
  (emacs-eglot-from-git
   #:name "emacs-eglot-git"
   #:version "1.7"
   #:revision "0"
   #:commit "55c13a91378cdd7822c99bbbf340ea76b1f0bf38"
   #:checksum "01861nbwkgx88ndhqcb2dcy9mzsk7za61ngbw02mxlg3ninl15ic"))

(define-public emacs-jupyter-git
  (let ((commit "1f0612eb936d36abab0f27b09cca691e81fc6e74")
        (revision "0"))
    (package
      (name "emacs-jupyter-git")
      (version "0.8.2")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/nnicandro/emacs-jupyter")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1mpch20iahijlgwg8bjpjg7bm9hd2wyskqbknafw8jkwyj7dvng2"))))
      (build-system emacs-build-system)
      (propagated-inputs
       `(("emacs-company" ,emacs-company)           ;optional
         ("emacs-markdown-mode" ,emacs-markdown-mode) ;optional
         ("emacs-simple-httpd" ,emacs-simple-httpd)
         ("emacs-websocket" ,emacs-websocket)
         ("emacs-zmq" ,emacs-zmq)))
      (home-page "https://github.com/nnicandro/emacs-jupyter")
      (synopsis "Emacs interface to communicate with Jupyter kernels")
      (description "This package provides an Emacs interface to communicate with
Jupyter kernels.  It provides REPL and @code{org-mode} source code block
frontends to Jupyter kernels and kernel interactions integrated with Emacs'
built-in features.")
      (license license:gpl3+))))

(define-public emacs-code-cells-git
  (let ((commit "f5150fc213da470da2d4fedaa4b86f476167b235")
        (revision "1"))
    (package
      (name "emacs-code-cells-git")
      (version (git-version "0.1" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/astoff/code-cells.el")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1p8a4ga6pysqd41frzzpi0a5bv1a0qmn136srfqqkkg60y5rlnl0"))))
      (build-system emacs-build-system)
      (home-page "https://github.com/nnicandro/emacs-jupyter")
      (synopsis "Emacs utilities for code split into cells, including Jupyter
notebooks")
      (description "This package lets you efficiently navigate, edit and
execute code split into cells according to certain magic comments.")
      (license license:gpl3+))))

(define-public emacs-consult-eglot-git
  (let ((commit "f93c571dc392a8b11d35541bffde30bd9f411d30")
        (revision "0"))
    (package
      (name "emacs-consult-eglot-git")
      (version "0.1")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/mohkale/consult-eglot")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1jqg6sg6iaqxpfn7symiy221mg9sn4y1rn0l1rw9rj9xmcnng7s0"))))
      (build-system emacs-build-system)
      (propagated-inputs
       `(("emacs-eglot-git" ,emacs-eglot-git)
         ("emacs-consult" ,emacs-consult)))
      (home-page "https://github.com/mohkale/consult-eglot")
      (synopsis "Jump to workspace symbols with eglot and consult")
      (description "A consulting-read interface for eglot.")
      (license license:gpl3+))))

(define-public emacs-tempel-git
  (let ((commit "6ca08f66585dee09d1e48ee600cbaab579720488")
        (revision "0"))
    (package
      (name "emacs-tempel-git")
      (version (git-version "0.2" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/minad/tempel")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "09l7scif2sc0m9ral5nzv4rf02pwsk7z5822y77klj36zsrxigdc"))))
      (arguments
       (list #:tests? #f))
      (build-system emacs-build-system)
      (home-page "https://github.com/minad/tempel")
      (synopsis "TempEl - Simple templates for Emacs")
      (description
       "Tempel is a tiny template package for Emacs, which uses the
  syntax of the Emacs Tempo library.  However Tempo may be a bit
  dusty here and there.  Therefore we present to you, Tempel, a
  modernized implementation of Tempo.")
      (license license:gpl3+))))

(define-public emacs-spell-fu-git
  ;; There are no tagged releases upstream on gitlab, instead we are using the
  ;; most recent commit.
  (let ((commit "50be652a6ec8590c3098f46094a92213623349c1")
        (revision "0"))
    (package
      (name "emacs-spell-fu-git")
      (version (git-version "0.3" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/ideasman42/emacs-spell-fu")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "0n7qwnirvkh2aprb7l1wj9rywdsn33a7s32716m3afcvy7z9pyh4"))))
      (build-system emacs-build-system)
      (home-page "https://gitlab.com/ideasman42/emacs-spell-fu")
      (synopsis "Fast highlighting of misspelled words")
      (description
       "This is a light weight spell checker for Emacs,
that runs from the syntax highlighter without starting external processes.")
      (license license:gpl3+))))

(define-public emacs-meow-git
  (let ((commit "845c4693ade5aa24715d3c0e4da678ca3fa98b51")
        (revision "0"))
    (package
      (name "emacs-meow-git")
      (version (git-version "1.4.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/meow-edit/meow")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "124afg43kxlizn69hfhd7w157i3c59ifap1ilfcpga3dgvhn3ws7"))))
      (build-system emacs-build-system)
      (home-page "https://github.com/meow-edit/meow")
      (synopsis "Yet another modal editing on Emacs")
      (description "Meow is yet another modal editing mode for Emacs.  It aims
  to blend modal editing into Emacs with minimum interface with its original
  key-bindings, avoiding most if not all the hassle introduced by key-binding
  conflicts.")
      (license license:gpl3+))))

(define-public emacs-zk-git
  (let ((commit "69e1e25188ad2bb925b12e1426a5afd9e11fa714")
        (revision "0"))
    (package
      (name "emacs-zk-git")
      (version (git-version "0.3" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/localauthor/zk")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "06lb9c7v1w6wxx9fyrjc1ax9zky1kc3wfbn3pbamwf6c4jjaz25r"))))
      (propagated-inputs
       `(("emacs-consult" ,emacs-consult)))
      (build-system emacs-build-system)
      (home-page "https://github.com/localauthor/zk")
      (synopsis "TBD")
      (description "TBD")
      (license license:gpl3+))))

(define-public emacs-org-modern-git
  (let ((commit "dc19304f409259d1b258c51cedd2d362e0ff9b98")
        (revision "0"))
    (package
      (name "emacs-org-modern-git")
      (version (git-version "0.1" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/minad/org-modern")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1b0cis1n786c4lkrsi71ak2wv21mhgbfk3q2pp6qiqhddah0l1cg"))))
      (build-system emacs-build-system)
      (home-page "https://github.com/minad/org-modern")
      (synopsis "TBD")
      (description "TBD")
      (license license:gpl3+))))
